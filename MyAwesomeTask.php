<?php

declare(strict_types=1);

/**
 * Thee bootstrap file of the plugin.
 * php version 7.3.0
 *
 * @category Plugin
 * @package  MyAwesomeTask
 * @author   Faiyaz Alam <example@example.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @link     www.example.com
 * @since    1.0.0
 */

namespace MyAwesomeTask;

use MyAwesomeTask\Inc\Core\Init;

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * Plugin Name:       My Awesome Task
 * Plugin URI:        https://github.com/faiyazalam/
 * Description:       My Awesome Task WordPress Plugin
 * Version:           1.0.0
 * Author:            Faiyaz Alam
 * Author URI:        https://github.com/faiyazalam/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       mat
 * Domain Path:       /languages
 *
 * @category Plugin
 * @package  MyAwesomeTask
 * @author   Faiyaz Alam <example@example.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @link     www.example.com
 * @since    1.0.0
 */
// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

/**
 * Define Constants
 */
define(__NAMESPACE__ . '\NS', __NAMESPACE__ . '\\');

define(NS . 'PLUGIN_NAME', 'my-awesome-task');

define(NS . 'PLUGIN_VERSION', '1.0.0');

define(NS . 'PLUGIN_NAME_DIR', plugin_dir_path(__FILE__));

define(NS . 'PLUGIN_NAME_URL', plugin_dir_url(__FILE__));

define(NS . 'PLUGIN_BASENAME', plugin_basename(__FILE__));

define(NS . 'PLUGIN_TEXT_DOMAIN', 'my-awesome-task');

/**
 * Autoload Classes
 */
require_once PLUGIN_NAME_DIR . 'Inc/Libraries/autoloader.php';

/**
 * Register Activation and Deactivation Hooks
 * This action is documented in inc/core/class-activator.php
 */
register_activation_hook(__FILE__, [NS . 'Inc\Core\Activator', 'activate']);

/**
 * The code that runs during plugin deactivation.
 * This action is documented inc/core/class-deactivator.php
 */
register_deactivation_hook(__FILE__, [NS . 'Inc\Core\Deactivator', 'deactivate']);

/**
 * Plugin Singleton Container
 *
 * Maintains a single copy of the plugin app object
 *
 * @category Plugin
 * @package  MyAwesomeTask
 * @author   Faiyaz Alam <example@example.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @link     www.example.com
 * @since    1.0.0
 */
class MyAwesomeTask
{

    /**
     * The instance of the plugin.
     *
     * @since 1.0.0
     * @var   Init $init Instance of the plugin.
     */
    private static $init;

    /**
     * Loads the plugin
     *
     * @category Plugin
     * @package  MyAwesomeTask
     * @author   Faiyaz Alam <example@example.com>
     * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
     * @link     www.example.com
     * @since    1.0.0
     *
     * @return MyAwesomeTask\Inc\Core\Init The instance of the plugin.
     */
    public static function init() : Init
    {
        if (null === self::$init) {
            self::$init = new Init();
            self::$init->run();
        }

        return self::$init;
    }
}

/**
 * Begins execution of the plugin
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * Also returns copy of the app object so 3rd party developers
 * can interact with the plugin's hooks contained within.
 *
 * @category Plugin
 * @package  MyAwesomeTask
 * @author   Faiyaz Alam <example@example.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @link     www.example.com
 * @since    1.0.0
 *
 * @return MyAwesomeTask\Inc\Core\Init The instance of the plugin.
 */
function matInit() : Init
{
    return MyAwesomeTask::init();
}

$phpVersion = '7.3.0';

// Check the minimum required PHP version and run the plugin.
if (version_compare(PHP_VERSION, $phpVersion, '>=')) {
    matInit();
}

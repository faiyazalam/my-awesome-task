<?php

declare(strict_types=1);

/**
 * Fired when the plugin is uninstalled.
 *
 * When populating this file, consider the following flow
 * of control:
 *
 * - This method should be static
 * - Check if the $_REQUEST content actually is the plugin name
 * - Run an admin referrer check to make sure it goes through authentication
 * - Verify the output of $_GET makes sense
 * - Repeat with other user roles. Best directly by using the links/query string
 * parameters.
 * - Repeat things for multisite. Once for a single site in the network, once
 * sitewide.
 *
 * This file may be updated more in future version of the Boilerplate; however,
 * this is the
 * general skeleton and outline for how the file should work.
 *
 * php version 7.3.0
 *
 * @category Plugin
 * @package  My_Awesome_Task
 * @author   Faiyaz Alam <example@example.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @link     www.example.com
 * @since    1.0.0
 */
// If uninstall not called from WordPress, then exit.
if (!defined('WP_UNINSTALL_PLUGIN')) {
    exit;
}

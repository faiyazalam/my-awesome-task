<?php

declare(strict_types=1);

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 * php version 7.3.0
 *
 * @category Plugin
 * @package  MyAwesomeTask
 * @author   Faiyaz Alam <example@example.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @link     www.example.com
 * @since    1.0.0
 */
use MyAwesomeTask as NS;
?>
<?php get_header() ?>

<h1><?php esc_html_e('User Listing', 'mat') ?></h1>

<table id="mat-user-listing-table" class="display" style="width:100%" 
       data-users_nonce="<?php echo esc_attr(wp_create_nonce('mat_users_nonce'))?>" 
       data-user_nonce="<?php echo esc_attr(wp_create_nonce('mat_user_nonce'))?>"
       >
    <thead>
        <tr>
            <th><?php esc_html_e('ID', 'mat') ?></th>
            <th><?php esc_html_e('Name', 'mat') ?></th>
            <th><?php esc_html_e('Username', 'mat') ?></th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th><?php esc_html_e('ID', 'mat') ?></th>
            <th><?php esc_html_e('Name', 'mat') ?></th>
            <th><?php esc_html_e('Username', 'mat') ?></th>
        </tr>
    </tfoot>
</table>

<?php require NS\PLUGIN_NAME_DIR . 'Inc/Frontend/views/elements/modal.php'; ?>

<?php get_footer(); ?>
<?php

declare(strict_types=1);

/**
 * Modal to show user details
 *
 * This file is used to markup the public-facing aspects of the plugin.
 * php version 7.3.0
 *
 * @category Plugin
 * @package  MyAwesomeTask
 * @author   Faiyaz Alam <example@example.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @link     www.example.com
 * @since    1.0.0
 */
?>
<!-- Modal -->
<div class="modal fade" id="mat-user-detail-modal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    &times;
                </button>
                <h4 class="modal-title"><?php esc_html_e('User Detail', 'mat') ?></h4>
            </div>
            <div class="modal-body">
                <!--Load content here via Ajax-->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <?php esc_html_e('Close', 'mat') ?>
                </button>
            </div>
        </div>

    </div>
</div>
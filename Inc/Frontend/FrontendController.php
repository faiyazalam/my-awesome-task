<?php

/**
 * Controls frontend side.
 * php version 7.3.0
 *
 * @category Plugin
 * @package  MyAwesomeTask
 * @author   Faiyaz Alam <example@example.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @link     www.example.com
 * @since    1.0.0
 */
declare(strict_types=1);

namespace MyAwesomeTask\Inc\Frontend;

use MyAwesomeTask as NS;

/**
 * The base controller for public-facing functionality of the plugin.
 *
 * @category Plugin
 * @package  MyAwesomeTask
 * @author   Faiyaz Alam <example@example.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @link     www.example.com
 * @since    1.0.0
 */
class FrontendController
{

    /**
     * The ID of this plugin.
     *
     * @category Plugin
     * @package  MyAwesomeTask
     * @author   Faiyaz Alam <example@example.com>
     * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
     * @link     www.example.com
     * @since    1.0.0
     */
    private $pluginName;

    /**
     * The version of this plugin.
     *
     * @category Plugin
     * @package  MyAwesomeTask
     * @author   Faiyaz Alam <example@example.com>
     * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
     * @link     www.example.com
     * @since    1.0.0
     */
    private $version;

    /**
     * The text domain of this plugin.
     *
     * @category Plugin
     * @package  MyAwesomeTask
     * @author   Faiyaz Alam <example@example.com>
     * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
     * @link     www.example.com
     * @since    1.0.0
     */
    private $pluginTextDomain;

    /**
     * Initialize the class and set its properties.
     *
     * @param string $pluginName       plugin name
     * @param string $version          plugin version
     * @param string $pluginTextDomain plugin text domain
     *
     * @category Plugin
     * @package  MyAwesomeTask
     * @author   Faiyaz Alam <example@example.com>
     * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
     * @link     www.example.com
     * @since    1.0.0
     */
    public function __construct(string $pluginName, string $version, string $pluginTextDomain)
    {
        $this->pluginName = $pluginName;
        $this->version = $version;
        $this->pluginTextDomain = $pluginTextDomain;
    }

    /**
     * Get the plugin name
     *
     * @since  1.0.0
     * @return string
     */
    protected function getPluginName() : string
    {
        return $this->pluginName;
    }

    /**
     * Get the plugin version
     *
     * @since  1.0.0
     * @return string
     */
    protected function getVersion() : string
    {
        return $this->version;
    }

    /**
     * Get the plugin text domain
     *
     * @since  1.0.0
     * @return string
     */
    protected function getPluginTextDomain() : string
    {
        return $this->pluginTextDomain;
    }

    /**
     * Get a generic message for server side error
     *
     * @since  1.0.0
     * @return string
     */
    protected function getServerErrorMessage() : string
    {
        return __('Something went wrong.', 'mat');
    }
}

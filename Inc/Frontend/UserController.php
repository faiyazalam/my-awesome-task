<?php

/**
 * Controls users from frontend side.
 * php version 7.3.0
 *
 * @category Plugin
 * @package  MyAwesomeTask
 * @author   Faiyaz Alam <example@example.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @link     www.example.com
 * @since    1.0.0
 */
declare(strict_types=1);

namespace MyAwesomeTask\Inc\Frontend;

use MyAwesomeTask as NS;
use MyAwesomeTask\Inc\Core\Logger;
use MyAwesomeTask\Inc\Core\Tool;
use MyAwesomeTask\Inc\Exception\NotFoundException;
use MyAwesomeTask\Inc\Core\Setting;

/**
 * Controls users from frontend side.
 *
 * @category Plugin
 * @package  MyAwesomeTask
 * @author   Faiyaz Alam <example@example.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @link     www.example.com
 * @since    1.0.0
 */
class UserController extends FrontendController
{

    /**
     * The API Endpoint
     */
    const API_BASE_URL = 'https://jsonplaceholder.typicode.com/';

    /**
     * Holds the default route
     */
    const DEFAULT_ROUTE = '/mat-users';

    /**
     * Holds the route
     *
     * @var string
     */
    private $route;

    /**
     * Get the route
     *
     * @return string
     */
    private function getRoute(): string
    {
        
        if (is_null($this->route)) {
            $settings = get_option(Setting::SETTING_NAME);
            $uri = !empty($settings['route']) ? $settings['route'] : self::DEFAULT_ROUTE;
            $this->route = Tool::cleanUri($uri);
        }
        
      

        return $this->route;
    }

    /**
     * Checks if current page is user listing page.
     *
     * @since  1.0.0
     * @return bool
     */
    private function isUserListingPage(): bool
    {
        return get_query_var('mat_pagename', false) !== false;
    }

    /**
     * Add a router for user listing page
     * Hooked with "init" action
     *
     * @return null
     * @since  1.0.0
     */
    public function addRewriteRules()
    {
        add_rewrite_rule(
            "^{$this->getRoute()}$",
            'index.php?mat_pagename=1',
            'top'
        );

        Tool::deleteFlushTransient();
    }

    /**
     * Loads a template for user listing route.
     * It loads a custom template 'template-custom.php'
     * if the file is located in theme directory.
     *
     * Hooked with 'template_include' filter
     *
     * @param string $template the template file name
     *
     * @since  1.0.0
     * @global WP_Query $wp_query
     *
     * @return string
     */
    public function loadTemplatesFoRewriteRules(string $template): string
    {
        if ($this->isUserListingPage()) {
            $themeFile = locate_template(['template-custom-mat.php']);
            $pluginFile = NS\PLUGIN_NAME_DIR . 'Inc/Frontend/views/template.php';
            $template = $themeFile ? $themeFile : $pluginFile;
            $template = apply_filters('the_mat_template', $template);
        }

        return $template;
    }

    /**
     * Changes a page title for user listing page.
     *
     * Hooked with 'document_title_parts' filter.
     *
     * @param array $data the data
     *
     * @since  1.0.0
     * @return array
     */
    public function changeTitlesForRewriteRules(array $data): array
    {
        if ($this->isUserListingPage()) {
            $data['title'] = "User Listing Page";
        }

        return $data;
    }

    /**
     * Fetches user list.
     *
     * Hooked with "wp_ajax_*" action
     *
     * @since 1.0.0
     *
     * @return json User listing
     */
    public function fetchUsers()
    {
        check_ajax_referer('mat_users_nonce', 'mat_nonce');
        $data = [];
        $apiUrl = self::API_BASE_URL . 'users';
        $response = wp_remote_get($apiUrl);

        if (!is_wp_error($response)) {
            $responseBody = wp_remote_retrieve_body($response);
            $data = json_decode($responseBody, true);

            foreach ($data as &$row) {
                unset($row['email']);
                unset($row['address']);
                unset($row['phone']);
                unset($row['website']);
                unset($row['company']);
            }

            $draw = filter_input(INPUT_GET, 'draw', FILTER_VALIDATE_INT);
            
            echo json_encode(
                [
                        'draw' => $draw,
                        'recordsTotal' => 10,
                        'recordsFiltered' => 10,
                        'data' => $data,
                    ]
            );
            exit;
        }
                 Logger::log('mat-user-listing', $response);
        $data['message'] = $this->getServerErrorMessage();
        wp_send_json_error($data, 500);
    }

    /**
     * Get user detail.
     *
     * Hooked with "wp_ajax_*" action
     *
     * @since 1.0.0
     *
     * @return json User detail
     */
    public function fetchUserDetail()
    {
        check_ajax_referer('mat_user_nonce', 'mat_nonce');
        
        $id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
        $data = [];
        try {
            $userData = $this->fetchUserDetailById($id);
            
            if (!is_null($userData)) {
                wp_send_json_success($userData);
            }
            
            $data['message'] = $this->getServerErrorMessage();
            wp_send_json_error($data, 500);
        } catch (NotFoundException $exc) {
            Logger::log('mat-user-detail', $exc);
            $data['message'] = $exc->getMessage();
            wp_send_json_error($data, 404);
        }
    }
    
    /**
     * Fetches a user by id.
     *
     * @param int $id The user id to be fetched
     * @return array
     * @throws NotFoundException
     *
     * @since 1.0.0
     * @return array|null  The array on success, null on failure
     */
    public function fetchUserDetailById(int $id) : ?array
    {
        $apiUrl = self::API_BASE_URL . 'users/' . $id;
        $response = wp_remote_get($apiUrl);

        if (is_wp_error($response)) {
            Logger::log('mat-user-detail-by-id', $response);
            return null;
        }
        
        $responseBody = wp_remote_retrieve_body($response);
        $data = json_decode($responseBody, true);

        if (empty($data)) {
            throw new NotFoundException("No user found with id='{$id}'.");
        }

        return $data;
    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since 1.0.0
     *
     * @return null
     */
    public function enqueueStyles()
    {
        if ($this->isUserListingPage()) {
            wp_enqueue_style(
                $this->getPluginName() . "-bootstrap.min",
                plugin_dir_url(__FILE__) . 'css/bootstrap.min.css',
                [],
                $this->getVersion(),
                'all'
            );

            wp_enqueue_style(
                $this->getPluginName() . "-jquery.dataTables.min",
                plugin_dir_url(__FILE__) . 'css/jquery.dataTables.min.css',
                [],
                $this->getVersion(),
                'all'
            );

            wp_enqueue_style(
                $this->getPluginName() . "-frontend",
                plugin_dir_url(__FILE__) . 'css/my-awesome-task-frontend.css',
                [],
                $this->getVersion(),
                'all'
            );
        }
    }

    /**
     * Register the JavaScript for the public-facing side of the site.
     *
     * @since 1.0.0
     *
     * @return null
     */
    public function enqueueScripts()
    {
        if ($this->isUserListingPage()) {
            wp_enqueue_script(
                $this->getPluginName() . "-bootstrap.min",
                plugin_dir_url(__FILE__) . 'js/bootstrap.min.js',
                ['jquery'],
                $this->getVersion(),
                false
            );

            wp_enqueue_script(
                $this->getPluginName() . "-jquery.dataTables.min",
                plugin_dir_url(__FILE__) . 'js/jquery.dataTables.min.js',
                ['jquery'],
                $this->getVersion(),
                false
            );

            wp_enqueue_script(
                $this->getPluginName() . "-frontend",
                plugin_dir_url(__FILE__) . 'js/my-awesome-task-frontend.js',
                ['jquery'],
                $this->getVersion(),
                false
            );

            wp_localize_script(
                $this->getPluginName() . '-frontend',
                'mat_custom_object',
                [
                        'ajax_url' => admin_url('admin-ajax.php'),
                        'server_error' => $this->getServerErrorMessage(),
                    ]
            );
        }
    }

    /**
     * Hooked with filter - query_vars.
     *
     * @param array $vars The query vars.

     * @since 1.0.0
     *
     * @return array
     */
    public function queryVars(array $vars) : array
    {
        $vars[] = 'mat_pagename';
        return $vars;
    }
}

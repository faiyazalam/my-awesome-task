(function ($) {
    'use strict';
    $(document).ready(
            function () {
                let table = $('#mat-user-listing-table');

                let render_with_link = function (data) {
                    return `<a href="#" class="show_details">${data}</a>`;
                }


                table.DataTable(
                        {
                            "ordering": false,
                            "bPaginate": false,
                            "bInfo": false,
                            "searching": false,
                            "serverSide": true,
                            "ajax": {
                                url: mat_custom_object.ajax_url, data: {
                                    action: "mat_get_users",
                                    mat_nonce: table.data('users_nonce')
                                },
                                error: function (xhr, status, errorThrown) {
                                    if (xhr.responseJSON) {
                                        let {data} = xhr.responseJSON;
                                        if (data) {
                                            alert(data.message);
                                            return;
                                        }
                                    }

                                    alert(errorThrown || mat_custom_object.server_error);
                                }
                            },
                            "columns": [
                                {"data": "id", render: render_with_link},
                                {"data": "name", render: render_with_link},
                                {"data": "username", render: render_with_link},
                            ],
                            'createdRow': function (row, data, dataIndex) {
                                $(row).attr('id', data.id);
                            },

                        }
                );

                let localCache = {
                    data: {},
                    isExist: function (id) {
                        return localCache.data.hasOwnProperty(id) && localCache.data[id] !== null;
                    },
                    get: function (id) {
                        return localCache.data[id];
                    },
                    set: function (id, xhr, callback) {
                        localCache.data[id] = xhr;
                        if ($.isFunction(callback)) {
                            callback(xhr);
                        }
                    }
                };

                table.on(
                        "click",
                        "a.show_details",
                        function (e) {
                            e.preventDefault();
                            let modalDiv = $("#mat-user-detail-modal");
                            let link = $(this);
                            let id = link.closest('tr').attr('id');

                            $.ajax(
                                    {
                                        url: mat_custom_object.ajax_url,
                                        data: {
                                            action: "mat_get_user_detail",
                                            id: id,
                                            mat_nonce: table.data('user_nonce')
                                        },
                                        cache: true,
                                        beforeSend: function () {
                                            link.addClass("running");
                                            if (localCache.isExist(id)) {
                                                link.removeClass("running");
                                                handleResponse(localCache.get(id));
                                                return false;
                                            }
                                            return true;
                                        },
                                        error: function (xhr, status, errorThrown) {
                                            if (xhr.responseJSON) {
                                                let {data} = xhr.responseJSON;
                                                if (data) {
                                                    alert(data.message);
                                                    return;
                                                }
                                            }

                                            alert(errorThrown || mat_custom_object.server_error);
                                        },
                                        complete: function (xhr, textStatus) {
                                            link.removeClass("running");
                                            localCache.set(id, xhr, handleResponse);
                                        }
                                    }
                            );

                            function handleResponse(xhr)
                            {
                                if (!xhr.responseJSON) {
                                    return;
                                }

                                let response = xhr.responseJSON;

                                if (response.success) {
                                    modalDiv.find("div.modal-body").html(get_detail_template(response.data));
                                    modalDiv.modal(
                                            {
                                                backdrop: 'static',
                                                keyboard: false
                                            }
                                    );
                                }
                            }
                        }
                );

                let get_detail_template = function (user) {
                    let {address} = user;
                    let {company} = user;

                    return `<table>
                <tbody>
                <tr>
                <th>ID</th>
                <td>&nbsp;</td>
                <td>${user.id}</td>
                </tr>
                <tr>
                <th>Name</th>
                <td>&nbsp;</td>
                <td>${user.name}</td>
                </tr>
                <tr>
                <th>Username</th>
                <td>&nbsp;</td>
                <td>${user.username}</td>
                </tr>
                <tr>
                <th>Email</th>
                <td>&nbsp;</td>
                <td>${user.email}</td>
                </tr>
                <tr>
                <th>Address</th>
                <td>&nbsp;</td>
                <td>${address.street}, ${address.suite}, ${address.city}, ${address.zipcode} <br> lat: ${address.geo.lat}, lng: ${address.geo.lng} </td>
                </tr>
                <tr>
                <th>Phone</th>
                <td>&nbsp;</td>
                <td>${user.phone}</td>
                </tr>
                <tr>
                <th>Website</th>
                <td>&nbsp;</td>
                <td>${user.website}</td>
                </tr>
                <tr>
                <th>Company</th>
                <td>&nbsp;</td>
                <td>${company.name}, ${company.catchPhrase} <br>${company.bs}</td>
                </tr>
                </tbody>
                </table>`;
                }

            }
    );

})(jQuery);

<?php

declare(strict_types=1);

/**
 * Dynamically loads the class attempting to be instantiated elsewhere in the
 * plugin by looking at the $className parameter being passed as an argument.
 * php version 7.3.0

 * The argument should be in the form: MyAwesomeTask\Namespace. The
 * function will then break the fully-qualified class name into its pieces and
 * will then build a file to the path based on the namespace.
 *
 * The namespaces in this plugin map to the paths in the directory structure.
 *
 * @param string $className The fully-qualified name of the class to load.
 *
 * @category Plugin
 * @package  MyAwesomeTask
 * @author   Faiyaz Alam <example@example.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @link     www.example.com
 * @since    1.0.0
 */

spl_autoload_register(
    function (string $className) {

        // If the specified $className does not include our namespace, duck out.
        if (false === strpos($className, 'MyAwesomeTask')) {
            return;
        }

        // Split the class name into an array to read the namespace and class.
        $fileParts = explode('\\', $className);

        // Do a reverse loop through $fileParts to build the path to the file.
        $namespace = '';
        for ($i = count($fileParts) - 1; $i > 0; $i--) {
            // Read the current component of the file part.
            $current = $fileParts[$i];

            // If we're at the first entry, then we're at the filename.
            if (count($fileParts) - 1 === $i) {
                    $fileName = "$current.php";
            } else {
                $namespace = '/' . $current . $namespace;
            }
        }

        // Now build a path to the file using mapping to the file location.
    
        $filepath =  rtrim(realpath((__DIR__."/../../")). $namespace, '/\\').'/';
        $filepath .= $fileName;

        require_once $filepath;
    }
);

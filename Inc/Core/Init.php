<?php

/**
 * Initializes with various hooks.
 * php version 7.3.0
 *
 * @category Plugin
 * @package  MyAwesomeTask
 * @author   Faiyaz Alam <example@example.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @link     www.example.com
 * @since    1.0.0
 */
declare(strict_types=1);

namespace MyAwesomeTask\Inc\Core;

use MyAwesomeTask as NS;
use MyAwesomeTask\Inc\Admin as Admin;
use MyAwesomeTask\Inc\Frontend as Frontend;

/**
 * The core plugin class.
 * Defines internationalization, admin-specific hooks, and public-facing site hooks.
 *
 * @category Plugin
 * @package  MyAwesomeTask
 * @author   Faiyaz Alam <example@example.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @link     www.example.com
 * @since    1.0.0
 */
class Init
{

    /**
     * The loader that's responsible for maintaining
     * and registering all hooks that power
     * the plugin.
     *
     * @since 1.0.0
     */
    protected $loader;

    /**
     * The unique identifier of this plugin.
     *
     * @since 1.0.0
     */
    protected $pluginBasename;

    /**
     * The current version of the plugin.
     *
     * @since 1.0.0
     */
    protected $version;

    /**
     * The text domain of the plugin.
     *
     * @since 1.0.0
     */
    protected $pluginTextDomain;

    /**
     * Initialize and define the core functionality of the plugin.
     *
     * @since 1.0.0
     */
    public function __construct()
    {
        $this->plugin_name = NS\PLUGIN_NAME;
        $this->version = NS\PLUGIN_VERSION;
        $this->pluginBasename = NS\PLUGIN_BASENAME;
        $this->pluginTextDomain = NS\PLUGIN_TEXT_DOMAIN;

        $this->loadDependencies();
        $this->setLocale();
        $this->defineAdminHooks();
        $this->definePublicHooks();
    }

    /**
     * Loads the following required dependencies for this plugin.
     *
     * - Loader - Orchestrates the hooks of the plugin.
     * - Internationalization_I18n - Defines internationalization functionality.
     * - Admin - Defines all hooks for the admin area.
     * - Frontend - Defines all hooks for the public side of the site.
     *
     * @since 1.0.0
     *
     * @return null
     */
    private function loadDependencies()
    {
        $this->loader = new Loader();
    }

    /**
     * Define the locale for this plugin for internationalization.
     *
     * Uses the Internationalization_I18n class in order to set the domain
     * and to register the hook
     * with WordPress.
     *
     * @since 1.0.0
     *
     * @return null
     */
    private function setLocale()
    {
        $intl = new Internationalization($this->pluginTextDomain);

        $this->loader->addAction(
            'plugins_loaded',
            $intl,
            'loadPluginTextdomain'
        );
    }

    /**
     * Register all of the hooks related to the admin area functionality
     * of the plugin.
     *
     * @since 1.0.0
     *
     * @return null
     */
    private function defineAdminHooks()
    {
        $adminController = new Admin\AdminController(
            $this->pluginName(),
            $this->version(),
            $this->pluginTextDomain()
        );

        $this->loader->addAction(
            'admin_enqueue_scripts',
            $adminController,
            'enqueueStyles'
        );

        $this->loader->addAction(
            'admin_enqueue_scripts',
            $adminController,
            'enqueueScripts'
        );

        $this->loader->addAction(
            'admin_menu',
            $adminController,
            'adminMenu'
        );

        $this->loader->addAction(
            'admin_init',
            $adminController,
            'settingsInit'
        );
        
        $this->loader->addAction(
            'update_option_'.Setting::SETTING_NAME,
            $adminController,
            'createFlushTransient'
        );
       
        $this->loader->addAction(
            'add_option_'.Setting::SETTING_NAME,
            $adminController,
            'createFlushTransient'
        );
    }

    /**
     * Register all of the hooks related to the public-facing functionality
     * of the plugin.
     *
     * @since 1.0.0
     *
     * @return null
     */
    private function definePublicHooks()
    {
        $userController = new Frontend\UserController(
            $this->pluginName(),
            $this->version(),
            $this->pluginTextDomain()
        );
        $this->loader->addAction(
            'wp_enqueue_scripts',
            $userController,
            'enqueueStyles'
        );
        $this->loader->addAction(
            'wp_enqueue_scripts',
            $userController,
            'enqueueScripts'
        );
        $this->loader->addAction(
            'init',
            $userController,
            'addRewriteRules'
        );
        $this->loader->addFilter(
            'template_include',
            $userController,
            'loadTemplatesFoRewriteRules'
        );
        $this->loader->addFilter(
            'document_title_parts',
            $userController,
            'changeTitlesForRewriteRules'
        );
        $this->loader->addAction(
            'wp_ajax_mat_get_users',
            $userController,
            'fetchUsers'
        );
        $this->loader->addAction(
            'wp_ajax_nopriv_mat_get_users',
            $userController,
            'fetchUsers'
        );
        $this->loader->addAction(
            'wp_ajax_mat_get_user_detail',
            $userController,
            'fetchUserDetail'
        );
        $this->loader->addAction(
            'wp_ajax_nopriv_mat_get_user_detail',
            $userController,
            'fetchUserDetail'
        );
        $this->loader->addAction(
            'query_vars',
            $userController,
            'queryVars'
        );
    }

    /**
     * Run the loader to execute all of the hooks with WordPress.
     *
     * @category Plugin
     * @package  MyAwesomeTask
     * @author   Faiyaz Alam <example@example.com>
     * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
     * @link     www.example.com
     * @since    1.0.0
     *
     * @return null
     */
    public function run()
    {
        $this->loader->run();
    }

    /**
     * The name of the plugin used to uniquely identify it within the context of
     * WordPress and to define internationalization functionality.
     *
     * @since 1.0.0
     *
     * @return string The plugin name
     */
    public function pluginName(): string
    {
        return $this->plugin_name;
    }

    /**
     * The reference to the class that orchestrates the hooks with the plugin.
     *
     * @since 1.0.0
     *
     * @return MyAwesomeTask\Inc\Core\Loader The loader
     */
    public function loader(): Loader
    {
        return $this->loader;
    }

    /**
     * Retrieve the version number of the plugin.
     *
     * @since 1.0.0
     *
     * @return string The plugin version
     */
    public function version(): string
    {
        return $this->version;
    }

    /**
     * Retrieve the text domain of the plugin.
     *
     * @since 1.0.0
     *
     * @return string The plugin text domain
     */
    public function pluginTextDomain(): string
    {
        return $this->pluginTextDomain;
    }
}

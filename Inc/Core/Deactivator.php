<?php

/**
 * Fired during plugin deactivation
 * php version 7.3.0
 *
 * @category Plugin
 * @package  MyAwesomeTask
 * @author   Faiyaz Alam <example@example.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @link     www.example.com
 * @since    1.0.0
 */
declare(strict_types=1);

namespace MyAwesomeTask\Inc\Core;

/**
 * Fired during plugin deactivation
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @category Plugin
 * @package  MyAwesomeTask
 * @author   Faiyaz Alam <example@example.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @link     www.example.com
 * @since    1.0.0
 * */
class Deactivator
{

    /**
     * Fired during plugin deactivation
     *
     * @since 1.0.0
     *
     * @return null
     */
    public static function deactivate()
    {
        //do nothing
    }
}

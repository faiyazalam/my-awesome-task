<?php

/**
 * A simple tool for the plugin
 * php version 7.3.0
 *
 * @category Plugin
 * @package  MyAwesomeTask
 * @author   Faiyaz Alam <example@example.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @link     www.example.com
 * @since    1.0.0
 */
declare(strict_types=1);

namespace MyAwesomeTask\Inc\Core;

/**
 * A simple tool for the plugin
 *
 * @category Plugin
 * @package  MyAwesomeTask
 * @author   Faiyaz Alam <example@example.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @link     www.example.com
 * @since    1.0.0
 */
class Tool
{
    
    const FLUSH_NAME = 'mat_flush';

    /**
     * Clear a URI by removing slash from staring only
     *
     * @param string $uri The URI to be cleaned.
     *
     * @since 1.0.0
     *
     * @return string
     */
    public static function cleanUri(string $uri): string
    {
        return trim($uri, "/");
    }

     /**
      * Create a flush transient for one minute.
      *
      * @since 1.0.0
      *
      * @return bool False if value was not set and true if value was set.
      */
    public static function createFlushTransient() : bool
    {
        return set_transient(self::FLUSH_NAME, 1, 60);
    }

    /**
     * Delete the flush transient and then flush the rewrite rules.
     *
     * @since 1.0.0

     * @return null
     */
    public static function deleteFlushTransient()
    {
        if (get_transient(self::FLUSH_NAME)) {
            delete_transient(self::FLUSH_NAME);
            flush_rewrite_rules();
        }
    }
}

<?php

declare(strict_types=1);

/**
 * A simple log for the plugin
 * php version 7.3.0
 *
 * @category Plugin
 * @package  MyAwesomeTask
 * @author   Faiyaz Alam <example@example.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @link     www.example.com
 * @since    1.0.0
 */

namespace MyAwesomeTask\Inc\Core;

/**
 * A simple log for the plugin
 * php version 7.3.0
 *
 * @category Plugin
 * @package  MyAwesomeTask
 * @author   Faiyaz Alam <example@example.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @link     www.example.com
 * @since    1.0.0
 */
class Logger
{

    /**
     * Log the message.
     *
     * @param string $filename The filename to be used for the log file.
     * @param mixed  $message  The message to be logged.
     *
     * @since 1.0.0
     *
     * @return bool
     */
    public static function log(string $filename, $message): bool
    {
        $filename = $filename . ".log" ?? "mat-log.log";
        ini_set('error_log', WP_CONTENT_DIR . DIRECTORY_SEPARATOR . $filename);

        if (\is_wp_error($message)) {
            $message = "Message: {$message->get_error_message()} "
                    . "and Code: {$message->get_error_code()}";
        } elseif (is_array($message)) {
            $message = implode(", ", $message);
        } elseif ($message instanceof \Exception) {
            $message = $message->getMessage() . "\n" . $message->getTraceAsString();
        }

        return error_log($message);
    }
}

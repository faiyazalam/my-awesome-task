<?php

/**
 * Initializes with various hooks.
 * php version 7.3.0
 *
 * @category Plugin
 * @package  MyAwesomeTask
 * @author   Faiyaz Alam <example@example.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @link     www.example.com
 * @since    1.0.0
 */
declare(strict_types=1);

namespace MyAwesomeTask\Inc\Core;

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @category Plugin
 * @package  MyAwesomeTask
 * @author   Faiyaz Alam <example@example.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @link     www.example.com
 * @since    1.0.0
 */
class Internationalization
{

    /**
     * The text domain of the plugin.
     *
     * @since 1.0.0
     */
    private $textDomain;

    /**
     * Initialize the class and set its properties.
     *
     * @param string $pluginTextDomain The plugin text domain
     *
     * @since 1.0.0
     */
    public function __construct(string $pluginTextDomain)
    {
        $this->textDomain = $pluginTextDomain;
    }

    /**
     * Load the plugin text domain for translation.
     *
     * @since 1.0.0
     *
     * @return null
     */
    public function loadPluginTextdomain()
    {
        load_plugin_textdomain(
            $this->textDomain,
            false,
            dirname(dirname(plugin_basename(__FILE__))) . '/languages/'
        );
    }
}

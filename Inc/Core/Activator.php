<?php

/**
 * Fired during plugin activation
 * php version 7.3.0
 *
 * @category Plugin
 * @package  MyAwesomeTask
 * @author   Faiyaz Alam <example@example.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @link     www.example.com
 * @since    1.0.0
 */
declare(strict_types=1);

namespace MyAwesomeTask\Inc\Core;

/**
 * Fired during plugin activation
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @category Plugin
 * @package  MyAwesomeTask
 * @author   Faiyaz Alam <example@example.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @link     www.example.com
 * @since    1.0.0
 * */
class Activator
{

    /**
     * Fires on plugin activation.
     *
     * @since 1.0.0
     *
     * @return null
     */
    public static function activate()
    {
        if (version_compare(PHP_VERSION, '7.3.0', '<')) {
            deactivate_plugins(plugin_basename(__FILE__));
           
            wp_die('Plugin requires a minmum PHP Version of 7.3.0');
        }

        Tool::createFlushTransient();
    }
}

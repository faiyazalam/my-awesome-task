<?php
/**
 * The admin-specific functionality of the plugin.
 * php version 7.3.0
 *
 * @category Plugin
 * @package  MyAwesomeTask
 * @author   Faiyaz Alam <example@example.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @link     www.example.com
 * @since    1.0.0
 */
declare(strict_types=1);

namespace MyAwesomeTask\Inc\Admin;

use MyAwesomeTask\Inc\Core\Setting;
use MyAwesomeTask\Inc\Core\Tool;

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @category Plugin
 * @package  MyAwesomeTask
 * @author   Faiyaz Alam <example@example.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @link     www.example.com
 * @since    1.0.0
 */
class AdminController
{

    /**
     * The ID of this plugin.
     *
     * @since 1.0.0
     */
    private $pluginName;

    /**
     * The version of this plugin.
     *
     * @since 1.0.0
     */
    private $version;

    /**
     * The text domain of this plugin.
     *
     * @since 1.0.0
     */
    private $pluginTextDomain;

    /**
     * Initialize the class and set its properties.
     *
     * @param string $pluginName       plugin name
     * @param string $version          plugin version
     * @param string $pluginTextDomain plugin text domain
     *
     * @since 1.0.0
     */
    public function __construct(string $pluginName, string $version, string $pluginTextDomain)
    {
        $this->pluginName = $pluginName;
        $this->version = $version;
        $this->pluginTextDomain = $pluginTextDomain;
    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since 1.0.0
     *
     * @return null
     */
    public function enqueueStyles()
    {
        wp_enqueue_style(
            $this->pluginName,
            plugin_dir_url(__FILE__) . 'css/my-awesome-task-admin.css',
            [],
            $this->version,
            'all'
        );
    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since 1.0.0
     *
     * @return null
     */
    public function enqueueScripts()
    {
        wp_enqueue_script(
            $this->pluginName,
            plugin_dir_url(__FILE__) . 'js/my-awesome-task-admin.js',
            ['jquery'],
            $this->version,
            false
        );
    }

    /**
     * Creates admin menu.
     * Hooked with action - admin_menu
     *
     * @since 1.0.0
     *
     * @return null
     */
    public function adminMenu()
    {
        add_options_page(
            __('My Awesome Task', 'mat'),
            __('My Awesome Task', 'mat'),
            'manage_options',
            'my-awesome-task',
            [$this, 'renderPage']
        );
    }

    /**
     * Registers settings.
     * Hooked with action - init.
     *
     * @since 1.0.0
     *
     * @return null
     */
    public function settingsInit()
    {
        register_setting('mat_settings_page', Setting::SETTING_NAME);

        add_settings_section(
            'mat_basic_settings',
            __('Basic Settings', 'mat'),
            [$this, 'settingsSectionCallback'],
            'mat_settings_page'
        );

        add_settings_field(
            'route',
            __('Enter new route', 'mat'),
            [$this, 'renderField'],
            'mat_settings_page',
            'mat_basic_settings'
        );
    }

    /**
     * Renders the setting field for route input.
     *
     * @since 1.0.0
     *
     * @return null
     */
    public function renderField()
    {
        $settingName = Setting::SETTING_NAME;
        $settings = get_option($settingName);
        ?>
        <input 
            type='text' 
            name="<?php echo esc_attr($settingName) ?>[route]" 
            value='<?php echo esc_attr($settings['route'])?? ""; ?>'
            >
        <?php
    }

    /**
     * Callback function for setting section.
     *
     * @since 1.0.0
     *
     * @return null
     */
    public function settingsSectionCallback()
    {
        echo esc_attr__('Manage route for user listing page', 'mat');
    }

    /**
     * The callback function to render settings page.
     *
     * @since 1.0.0
     *
     * @return null
     */
    public function renderPage()
    {
        ?>
        <form action='options.php' method='post'>

            <h2><?php esc_html_e('My Awesome Task', 'mat') ?> 
                - <?php esc_html_e('Settings', 'mat') ?></h2>

            <?php
            settings_fields('mat_settings_page');
            do_settings_sections('mat_settings_page');
            submit_button();
            ?>

        </form>
        <?php
    }

    /**
     * Set a transient to flush the rewrite rules
     *
     * @since 1.0.0
     * @return null
     */
    public function createFlushTransient()
    {
        Tool::createFlushTransient();
    }
}

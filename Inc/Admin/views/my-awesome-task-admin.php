<?php

declare(strict_types=1);

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * php version 7.3.0

 * @category Plugin
 * @package  My_Awesome_Task
 * @author   Faiyaz Alam <example@example.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.en.html GPL
 * @link     www.example.com
 * @since    1.0.0
 */

?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

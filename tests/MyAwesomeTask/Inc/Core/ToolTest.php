<?php

use MyAwesomeTask\Inc\Core\Tool;

class ToolTest extends \PHPUnit\Framework\TestCase {
    
    const URI = "/test/";

    public function test_shouldWork() {
        $newUri = Tool::cleanUri(self::URI);
        $this->assertTrue("test" === $newUri);
    }

    public function test_shouldNotWork() {
        $newUri = Tool::cleanUri(self::URI);
        $this->assertFalse("/test" === $newUri);
    }

    public function test_shouldNotWorkAgain() {
        $newUri = Tool::cleanUri(self::URI);
        $this->assertFalse("test/" === $newUri);
    }

}

# My Awesome Task - A WordPress Plugin
The plugin displays a list of random users on a virtual page. By virtual page, I mean that there is no need to create any page in WordPress. Just install the plugin and visit the default route i.e. `www.example.com/mat-users`. That's all you need to do.:) Optionally, you can also change the default route from the plugin settings page. 

The plugin uses a free third party API service to fetch a list of random users. For more information, please click [here](https://jsonplaceholder.typicode.com/).

## Requirements
* WordPress Version 5.4 or greater
* PHP Version 7.2 or greater
* Tested up to WordPress Version 5.4.2
* (Optional) [Composer](https://getcomposer.org/) to install dependencies.

## Dependencies
The plugin needs the following two optional dependencies:
* [PHPUnit](https://packagist.org/packages/phpunit/phpunit)
* [PHP Coding Standards (PHPCS)](https://packagist.org/packages/inpsyde/php-coding-standards)


## Installation
 1. Go to your WordPress plugins directroy at: `your-awesome-project/wp-content/plugins`
 2. Run the below command to clone the git repository: 
```
$ git clone https://gitlab.com/faiyazalam/my-awesome-task.git
```
If you do not have git installed in your system, you can download the plugin from [here](https://gitlab.com/faiyazalam/my-awesome-task/archive/master.zip) and then extract it in your WordPress pluign directory and then rename the extracted folder from `my-awesome-task-master` to `my-awesome-task`.
 3. (Optional) Run the following two commands to install the dependencies:
 ```
 $ cd my-awesome-task
 $ composer update
 ```
 4. Activate the plugin through the `Plugins` screen in WordPress
 4. (Optional) Use the settings `Settings >> My-Awesome-Task` screen to change the  default route.

## Usage
 1. If you did not change the default route, just visit the URL: `www.example.com/mat-users` to see the list of random users.
 2. If you changed the default route (let's say your new route is `/your-custom-route`), visit the URL: `www.example.com/your-custom-route` to see the list of random users.
 3. To see the detail of a user, just click on the username link on the table. 
 4. When you see the detail of a user first time, it saves the details of the user in the cache. To clear the cache, just relaod the page.
 5. To override the template, just create a template file `template-custom-mat.php` in your theme and then copy the content of the plugin template file located at `your-awesome-project/wp-content/plugins/my-awesome-task/inc/frontend/views/template.php` into your template file.
 6. You can also override the template by using the filter hook `the_mat_template`. Here is an example code snippet:
 ```
 add_filter('the_mat_template', 'custom_mat_template');
function custom_mat_template($template) {
     $template = 'your/template/file/path.php';
     return $template;
}
 ```
 7. To run PHPUnit tests and PHPCS, run the following commands from the plugin directory i.e `your-awesome-project/wp-content/plugins/my-awesome-task`:
 ```
$ vendor/bin/phpunit <file-path>
$ vendor/bin/phpcs  --standard="Inpsyde" <file-path>
$ vendor/bin/phpcbf --standard="Inpsyde" <file-path>
 ```
 
 

## Bug Fixes
If you find any bug, please create an issue with a step by step description to reproduce the bug. Please search the forum before creating a new issue.

## Copyright and License
Copyright (C) 2020 [Faiyaz Alam](https://github.com/faiyazalam)  
For licensing details, please visit [LICENSE.md](LICENSE.md)  


